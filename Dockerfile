FROM amazoncorretto:17-alpine-jdk
EXPOSE 8082
COPY target/auth-0.0.1.jar auth.jar
ENTRYPOINT ["java", "-jar", "auth.jar"]
