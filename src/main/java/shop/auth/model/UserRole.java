package shop.auth.model;

public enum UserRole {
	ADMIN, CUSTOMER
}
