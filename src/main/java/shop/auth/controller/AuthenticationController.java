package shop.auth.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.auth.model.UserRole;
import shop.auth.service.AuthenticationService;

@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

	private final AuthenticationService service;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/register")
	public ResponseEntity<String> register(
			@RequestBody RegisterRequest request) {
		return service.register(request, UserRole.CUSTOMER);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/login")
	public ResponseEntity<String> login(
			@RequestBody AuthenticationRequest request) {
		return service.login(request);
	}
}
