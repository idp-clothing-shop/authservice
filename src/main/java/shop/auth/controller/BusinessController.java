package shop.auth.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.auth.model.UserDTO;
import shop.auth.service.BusinessService;

@RestController
@RequestMapping("api/auth/business")
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
public class BusinessController {

	private final BusinessService service;

	@GetMapping
	public ResponseEntity<UserDTO> getUser() {
		return service.validateUser();
	}
}
