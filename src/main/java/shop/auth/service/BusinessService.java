package shop.auth.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import shop.auth.model.User;
import shop.auth.model.UserDTO;
import shop.auth.model.UserDTOMapper;

@Service
@RequiredArgsConstructor
public class BusinessService {

	private final UserDTOMapper userDTOMapper;

	public ResponseEntity<UserDTO> validateUser() {
		Authentication authentication = SecurityContextHolder
				.getContext()
				.getAuthentication();

		if (authentication instanceof AnonymousAuthenticationToken) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new UserDTO());
		}

		User user = (User)authentication.getPrincipal();

		return ResponseEntity.ok(userDTOMapper.apply(user));
	}
}
