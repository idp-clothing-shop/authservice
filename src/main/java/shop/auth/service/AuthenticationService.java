package shop.auth.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.auth.config.JwtService;
import shop.auth.controller.AuthenticationRequest;
import shop.auth.controller.RegisterRequest;
import shop.auth.model.User;
import shop.auth.model.UserRole;
import shop.auth.repository.UserRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

	private final UserRepository repository;
	private final PasswordEncoder passwordEncoder;
	private final JwtService jwtService;
	private final AuthenticationManager authenticationManager;

	public ResponseEntity<String> register(RegisterRequest request, UserRole role) {
		Authentication authentication = SecurityContextHolder
				.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body("You are already logged in");
		}

		Optional<User> opt = repository.findByEmail(request.getEmail());
		if (opt.isPresent()) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body("Email is taken");
		}

		var user = User.builder()
				.firstName(request.getFirstName())
				.lastName(request.getLastName())
				.email(request.getEmail())
				.password(passwordEncoder.encode(request.getPassword()))
				.userRole(role)
				.build();
		repository.save(user);
		return ResponseEntity.ok("Registered user");
	}

	public ResponseEntity<String> login(AuthenticationRequest request) {
		Authentication authentication = SecurityContextHolder
				.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body("You are already logged in");
		}

		Optional<User> opt = repository.findByEmail(request.getEmail());
		if (opt.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("User with given email does not exist");
		}
		User user = opt.get();

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							request.getEmail(),
							request.getPassword()));
		} catch (AuthenticationException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("Authentication failed");
		}

		Map<String, Object> claims = new HashMap<>();
		claims.put("Role", user.getUserRole());

		var jwtToken = jwtService.generateToken(claims, user);
		return ResponseEntity.status(HttpStatus.OK)
						.body(jwtToken);
	}
}
