package shop.auth.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import shop.auth.model.User;
import shop.auth.model.UserRole;
import shop.auth.repository.UserRepository;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AddAdminUsers implements CommandLineRunner {

	private final UserRepository repository;
	private final PasswordEncoder passwordEncoder;
	@Value("${security.admin.password}")
	private String adminPassword;

	@Override
	public void run(String... args) throws Exception {
		Optional<User> opt = repository.findByEmail("admin@gmail.com");
		if (opt.isEmpty()) {
			var user = User.builder()
					.firstName("Ana")
					.lastName("Nastase")
					.email("admin@gmail.com")
					.password(passwordEncoder.encode(adminPassword))
					.userRole(UserRole.ADMIN)
					.build();
			repository.save(user);
		}

		opt = repository.findByEmail("test.admin@gmail.com");
		if (opt.isEmpty()) {
			var user = User.builder()
					.firstName("Test")
					.lastName("Test")
					.email("test.admin@gmail.com")
					.password(passwordEncoder.encode("admin"))
					.userRole(UserRole.ADMIN)
					.build();
			repository.save(user);
		}
	}
}
